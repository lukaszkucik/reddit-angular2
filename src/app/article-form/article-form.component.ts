import {Component, EventEmitter, Output} from '@angular/core';
import {Article} from "../article/article.model";

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.css']
})
export class ArticleFormComponent {

  @Output() articleCreatedEvent = new EventEmitter<Article>();

  constructor() {
  }

  createArticle(title: HTMLInputElement, link: HTMLInputElement): boolean {
    this.articleCreatedEvent.emit(new Article(title.value, link.value, 0));
    title.value = '';
    link.value = '';
    return false;
  }
}
