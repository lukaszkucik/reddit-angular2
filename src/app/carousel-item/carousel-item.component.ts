import {Component} from '@angular/core';

@Component({
  selector: 'carousel-item',
  templateUrl: './carousel-item.component.html',
  styleUrls: ['./carousel-item.component.css']
})
export class CarouselItemComponent {

  isActive: boolean = false;

  get active() {
    return this.isActive ? 'carousel-item text-center active' : 'carousel-item text-center';
  }
}
