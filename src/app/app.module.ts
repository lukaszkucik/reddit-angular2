import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {ArticleComponent} from './article/article.component';
import {ArticleFormComponent} from './article-form/article-form.component';
import { CarouselComponent } from './carousel/carousel.component';
import { CarouselItemComponent } from './carousel-item/carousel-item.component';


@NgModule({
  declarations: [
    AppComponent,
    ArticleComponent,
    ArticleFormComponent,
    CarouselComponent,
    CarouselItemComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
