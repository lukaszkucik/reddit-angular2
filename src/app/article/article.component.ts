import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Article} from "./article.model";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css'],
  host: {
    class: 'row'
  }
})
export class ArticleComponent {

  @Input() article: Article;

  @Output() articleDeletedEvent = new EventEmitter<Article>();

  constructor() {
  }

  voteUp(): boolean {
    this.article.voteUp();
    return false;
  }

  voteDown(): boolean {
    this.article.voteDown();
    return false;
  }

  delete(): boolean {
    this.articleDeletedEvent.emit(this.article);
    return false;
  }
}
